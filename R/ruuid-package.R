##' ruuid
##'
##' Tools for Generating and Handling of UUIDs
##'
##' @name ruuid-package
##' @aliases ruuid
##' @docType package
##' @author \email{dicko.ahmadou@gmail.com}
##' @keywords package
NULL

