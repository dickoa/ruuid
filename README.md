
# ruuid

<!-- README.md is generated from README.Rmd. Please edit that file -->

[![GitLab CI Build
Status](https://gitlab.com/dickoa/ruuid/badges/master/build.svg)](https://gitlab.com/dickoa/ruuid/pipelines)
[![Codecov Code
Coverage](https://codecov.io/gl/dickoa/ruuid/branch/master/graph/badge.svg)](https://codecov.io/gl/dickoa/ruuid)
[![License:
MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

`ruuid` is an R client for the Humanitarian Exchange Data
platform.

![](https://gitlab.com/dickoa/ruuid/raw/5ec77127ad1a7322c6ff118f4b0a8fdcbba71788/inst/demo/demo.gif)

## Installation

This package is not on yet on CRAN and to install it, you will need the
[`devtools`](https://github.com/r-lib/devtools) package.

``` r
## install.packages("devtools") 
devtools::install_git("https://gitlab.com/dickoa/ruuid")
```

## ruuid tutorial

``` r
library("ruuid")
```

## Meta

  - Please [report any issues or
    bugs](https://gitlab.dickoa/ruuid/issues).
  - License: MIT
  - Please note that this project is released with a [Contributor Code
    of Conduct](CONDUCT.md). By participating in this project you agree
    to abide by its terms.
